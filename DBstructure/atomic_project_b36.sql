-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2016 at 12:35 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `birthday` date NOT NULL,
  `is_deleted` varchar(300) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `is_deleted`) VALUES
(1, 'Muhammad Iqbal hossain', '1993-12-16', 'No'),
(2, 'Muhammad Sakib', '1997-10-12', '2016-11-20 02:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(56) NOT NULL,
  `book_title` varchar(50) NOT NULL,
  `is_deleted` varchar(300) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `author_name`, `book_title`, `is_deleted`) VALUES
(1, 'Humayun Ahmed', 'Himu', 'No'),
(3, 'iqbal', 'Himuuuuu', 'No'),
(6, 'kdlhfsahda', 'ahsahdsdh', 'No'),
(7, '5685468746879', 'fsfaf', 'No'),
(8, '646748', 'alkdsahd', 'No'),
(10, '74558377415428', 'gdgdsg', 'No'),
(11, 'Humayun', 'Himu', 'No'),
(12, '54687687567', 'dddddd', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `is_deleted` varchar(300) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `is_deleted`) VALUES
(2, 'Muhammad sakib', 'Dhaka', 'No'),
(3, 'Mishu', 'Dhaka', '2016-11-20 04:35:50');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `email` varchar(40) NOT NULL,
  `is_deleted` varchar(300) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `is_deleted`) VALUES
(1, 'Muhammad Iqbal ', '', 'No'),
(2, 'Muhammad sakib', 'iqbal_2020@yahoo.com', 'No'),
(4, 'Avijit Barua', 'dhiman.hb@gmail.com', '2016-11-20 15:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `is_deleted` varchar(300) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `is_deleted`) VALUES
(1, 'Muhammad Iqbal hossain', 'Male', 'No'),
(3, 'Avijit SEN', 'Other', '2016-11-20 18:12:24'),
(4, 'Dhiman ', 'Male', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `hobbies` varchar(100) NOT NULL,
  `is_deleted` varchar(300) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_deleted`) VALUES
(1, 'Muhammad Iqbal hossain', 'Gardening', 'No'),
(2, 'Muhammad sakib', 'Angling', 'No'),
(3, 'Dhiman ', 'r', 'No'),
(4, 'Mishu', 'd', 'No'),
(5, 'Thusar', 'swimming', 'No'),
(6, 'Mishu', 'reading,dancing', 'No'),
(7, 'tttt', 'swimming,running', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profilepic`
--

CREATE TABLE IF NOT EXISTS `profilepic` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `profilePicture` varchar(100) NOT NULL,
  `is_deleted` varchar(300) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `profilepic`
--

INSERT INTO `profilepic` (`id`, `name`, `profilePicture`, `is_deleted`) VALUES
(9, 'tttt', '1478949618booktitle.jpg', '2016-11-20 20:35:11'),
(10, 'Avijit Barua', '1478978222butler-inn20161010003300.jpg', 'No'),
(11, 'Dhiman Barua', '1479646748dhi.jpg', 'No'),
(12, 'Ami Barua', '147965164214712806_1179203422154845_2485607284247319094_o.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE IF NOT EXISTS `summary_of_organization` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `summaryOfOrganization` varchar(100) NOT NULL,
  `is_deleted` varchar(300) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `name`, `summaryOfOrganization`, `is_deleted`) VALUES
(1, 'Muhammad Iqbal', 'BITM', 'No'),
(2, 'Muhammad Sakib', 'Google', '2016-11-21 00:49:10');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
