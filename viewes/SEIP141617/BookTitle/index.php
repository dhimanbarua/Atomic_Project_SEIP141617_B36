<html>
<head>
    <title>Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/jquery-ui.css">
    <script src="../../../Resources/Bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->
</head>
<body>


<table>
    <tr>
        <td width="600">
            <h2 text-align="center">Active List of Book Titles</h2>
        </td>
        <td width="500">
            <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
            <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
            <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>
        </td>
        <td width="200">


            <!-- required for search, block 4 of 5 start -->


            <form align="right" id="searchForm" action="index.php"  method="get">
                <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
                <input type="checkbox"  name="byTitle"   checked  >By Title
                <input type="checkbox"  name="byAuthor"  checked >By Author
                <input hidden type="submit" class="btn-primary" value="search">
            </form>

            <!-- required for search, block 4 of 5 end -->


        </td>
    </tr>
</table>


<?php

require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;


$objBookTitle = new BookTitle();

$allData = $objBookTitle->index("obj");
$serial = 1;

################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $objBookTitle->search($_REQUEST);
$availableKeywords=$objBookTitle->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

echo "<table class='table-class' border='5px'>";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Book Title </th>";
echo "<th> Author Name </th>";
echo "<th> Action </th>";


foreach($allData as $oneData){
    echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->book_title."</td>";
    echo "<td>".$oneData->author_name."</td>";


    echo "<td>";

    echo "<a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a> ";
    echo "<a href='edit.php?id=$oneData->id'><button class='btn btn-primary'>Edit</button></a> ";
    echo "<a href='trash.php?id=$oneData->id'><button class='btn btn-success'>Trash</button></a> ";
    echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";

?>
<?php



######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################



?>

<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="center" class="container">
    <ul class="pagination">

        <?php

        $pageMinusOne  = $page-1;
        $pagePlusOne  = $page+1;
        if($page>$pages) Utility::redirect("index.php?Page=$pages");

        if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->


</body>
</html>

<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->